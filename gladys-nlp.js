const NlpjsTFr = require('nlp-js-tools-french');
const posGoogle = require('./pos-google');
const intentGoogle = require('./intents-google');
const intent = require('./intents');

module.exports = function (RED) {
    "use strict";

    function Nlp(config) {
        let node = this;
        RED.nodes.createNode(node, config); // à garder en début de fonction

        function noStatus() {
            setTimeout(function() {
                node.status({});
            }, 200);
        }

        node.googleAuth = RED.nodes.getNode(config.googleAuth);

        // node.log("gladys-nlp' config " + JSON.stringify(config));

        node.on('input', function (msg) {
            // node.log("Something happened");
            // node.warn("Something happened you should know about");
            // node.error("Oh no, something bad happened");
            // node.error("Oh no, something bad happened", msg); // trigger node !Catch in flow
            // node.trace("Log some internal detail not needed for normal operation");
            // node.debug("Log something more details for debugging the node's behaviour");
            // node.log("gladys-nlp'd received " + JSON.stringify(msg));

            node.status({
                fill: "green",
                shape: "dot",
                text: "thinking"
            });

            let nlpToolsFr;
            switch (config.method) {
                case 'token':
                case 'tokens':
                case 'tokenized':
                    nlpToolsFr = new NlpjsTFr(msg.payload, msg);
                    send(nlpToolsFr.tokenized);
                    break;
                case 'pos':
                    nlpToolsFr = new NlpjsTFr(msg.payload, msg);
                    send(nlpToolsFr.posTagger());
                    break;
                case 'posGoogle':
                    posGoogle(node, node.googleAuth.key, msg.payload).then(send);
                    break;
                case 'lemmatizer':
                    nlpToolsFr = new NlpjsTFr(msg.payload, msg);
                    send(nlpToolsFr.lemmatizer());
                    break;
                case 'stemmer':
                    nlpToolsFr = new NlpjsTFr(msg.payload, msg);
                    send(nlpToolsFr.stemmer());
                    break;
                case 'intent':
                    send(intent(msg.payload));
                    break;
                case 'intentGoogle':
                    send(intentGoogle(node, msg.payload));
                    break;
            }

            function send(payload) {
                msg = {topic: config.method, payload: payload, text: msg.text};
                node.send(msg);
                noStatus();
            }

            // node.send([msg1, msg2]);
            // node.send([msg1, msg2], msg3);
        });

        node.on('close', function (removed, done) {
            if (removed) {
                // This node has been deleted
            } else {
                // This node is being restarted
            }
            done();
        });

    }

    RED.nodes.registerType("gladys-nlp", Nlp);
};
