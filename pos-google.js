const language = require('@google-cloud/language');

let api;

module.exports = function (node, auth, text) {
    if (!api) {
        try {
            console.log("FSI auth", auth);
            api = new language.v1beta2.LanguageServiceClient({
                credentials: auth
            });
        } catch (error) {
            node.status({fill: "red", shape: "dot", text: "error"});
            node.error(error);
            return Promise.reject(error);
        }
    }

    const document = {
        content: text,
        language: "fr",
        type: 'PLAIN_TEXT',
    };

    return api.annotateText({
        document: document,
        features: {
            extractSyntax: true,
            extractEntities: true,
            // extractDocumentSentiment: true,
            // extractEntitySentiment: true, // pas supporté pour le fr
            // classifyText: true // pas supporté pour le fr
        }
    })
        .then(results => {
            return results[0];
        });
};


