module.exports = intents;

function intents(node, pos) {

    let action;
    pos.tokens.forEach(function (token) {
        if (token.dependencyEdge.label === "ROOT" && token.partOfSpeech.tag === "VERB" && token.partOfSpeech.mood === "IMPERATIVE") {
            action = token;
        }
    });
    if (!action) {
        node.error("La phrase doit commencer par un verbe à l'impératif.");
        return {
            action: "dire",
            thing: "Comment ?"
        };
    }

    // donne son index à chaque token pour faciliter les accès
    pos.tokens.forEach(function (token, index) {
        token.index = index;
    });

    // donne l'entity's type aux tokens qui ont été identifiés comme entity
    const entityToType = {};
    pos.entities.forEach(function (entity) {
        entityToType[entity.name] = entity.type;
    });
    pos.tokens.forEach(function (token) {
        token.entityType = entityToType[token.text.content] || null;
    });

    console.log(JSON.stringify(pos, null, 2));

    // trouve le complément d'objet
    let thing;
    pos.tokens.forEach(function (token) {
        if (token === action) {
            return;
        }
        if (token.dependencyEdge.headTokenIndex === action.index && token.dependencyEdge.label.match(/^(D|P)OBJ|ATTR$/)) {
            thing = token;
        }
    });

    // trouve les qualificatifs du complément
    const tags = [];
    if (thing) {
        pos.tokens.forEach(function (token) {
            if (token === action || token === thing) {
                return;
            }
            if (token.dependencyEdge.headTokenIndex === thing.index && token.dependencyEdge.label.match(/^AMOD$/)) {
                tags.push(token.text.content);
                tags.push(token.lemma);
            }
        });
    }

    const intent = {
        action: action.lemma
    };
    if (thing) {
        intent.thing = thing.text.content;
    }
    intent.tags = tags;

    return intent;
}