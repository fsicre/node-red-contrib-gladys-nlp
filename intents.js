module.exports = function intents(tags) {
    const patterns = [
        [
            {action: /VER/},
            {ignore: /PRO/},
            {ignore: /ART/},
            {thing: /NOM|UNK/},
        ],
        [
            {action: /VER/},
            {ignore: /ART|PRO/},
            {thing: /NOM|UNK/},
        ],
        [
            {action: /VER/},
            {thing: /NOM|UNK/},
        ],
    ];

    const intents = [];
    const nTags = tags.length;

    patterns.forEach(function (pattern) {
        // console.log("pattern", pattern);
        for (let base = 0; base < nTags; base++) {
            // console.log();
            // console.log("base", base);
            const intent = {};
            let matches = 0;
            pattern.forEach(function (rules, index) {
                if ((base + index) > tags.length - 1) {
                    return;
                }
                let tag = tags[base + index];
                if (tag.used) {
                    return;
                }
                // console.log("index", index);
                // console.log("tag", tag);
                for (let prop in rules) {
                    let reg = rules[prop];
                    tag.pos.forEach(function (value) {
                        if (value.match(reg)) {
                            if (intent[prop] instanceof Array) {
                                if (intent[prop][intent[prop].length - 1].word !== tag.word) {
                                    intent[prop].push(tag);
                                }
                            }
                            else if (intent[prop]) {
                                if (intent[prop].word !== tag.word) {
                                    intent[prop] = [intent[prop], tag];
                                }
                            }
                            else {
                                intent[prop] = tag;
                            }
                            matches++;
                        }
                    });
                }
            });
            // console.log("intent", intent);
            // console.log("matches", matches);
            if (matches >= pattern.length) {
                // console.log("good", intent);
                for (let prop in intent) {
                    intent[prop].used = true;
                }
                intents.push(intent);
                base += matches;
            } else {
                // console.log("plouf");
            }
        }
    });

    return new Promise(function (resolve, reject) {
        resolve(intents);
    });
};